const r = require('rethinkdb');

const desc = r.desc('id');

const connection = (req, res, next) => {
  let count = 0;

  (function _connect() {
    r.connect({ host: 'localhost', port: 28015, db: 'prueba' }, (error, connec) => {
      if (error && error.name === 'ReqlDriverError' && error.message.indexOf('Could not connect') === 0 && ++count < 31) {
        console.log(error);
        setTimeout(_connect, 1000);
        return;
      }

      req.rdb = connec;
      next();
    });
  }());
};

const close = (req, res, next) => {
  req.rdb.close();
};

const rethinkdb = r.db('prueba');

module.exports = {
  rethinkdb,
  desc,
  connection,
  close,
};
