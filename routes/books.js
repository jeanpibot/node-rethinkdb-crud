const express = require('express');

const router = express.Router();

// database
const { rethinkdb, desc } = require('../lib/rethinkdb.js');

// Create book
router.post('/', async (req, res) => {
  try {
    const datos = req.body;
    await rethinkdb.table('books').insert(datos).run(req.rdb);
    res.status(201).send('Book was added!');
  } catch (error) {
    res.status(400).json(error);
  }
});
// get all of Books
router.get('/', async (req, res) => {
  try {
    const allBooks = await rethinkdb.table('books').orderBy(desc).run(req.rdb);
    res.status(200).json(allBooks);
  } catch (error) {
    console.error(error.message);
  }
});
// get a book
router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const getBook = await rethinkdb.table('books').get(id).run(req.rdb);
    res.status(200).json(getBook);
  } catch (error) {
    console.error(error.message);
  }
});
// update a book
router.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const datos = req.body;
    await rethinkdb.table('books').get(id).update(datos).run(req.rdb);
    res.status(200).send('Book was updated!');
  } catch (error) {
    res.status(400).json(error.message);
  }
});
// delete a book
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    await rethinkdb.table('books').get(id).delete().run(req.rdb);
    res.status(200).send('Book was deleted!');
  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = router;
